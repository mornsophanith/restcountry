import React, { useState } from 'react';
import '../country.css';
import { useNavigate,NavLink } from 'react-router-dom';

function Detail() {
  const queryParameters = new URLSearchParams(window.location.search)
  const queryParams = queryParameters.get("name")
  const navigate = useNavigate();
  const [countries, setCountries] = useState([]);
  React.useEffect(() => {
      fetch(`https://restcountries.com/v3.1/name/${queryParams}?fullText=true`)
        .then((res) => res.json())
        .then((data) => {
          if (data) {
            setCountries(data);
          }
          
      })
      // eslint-disable-next-line
  }, []);
 
  function handleClick(event) {
    navigate('/');
  }

  return (<>
    <header class="header-container">
      <div class="header-content">
        <h2 class="title"><a href="/">Where in the world?</a></h2>
        <p><i class="fa-regular fa-moon"></i>&nbsp;&nbsp;Dark Mode</p>
      </div>
    </header>
    <main>
      <div class="country-details-container">
        <span class="back-button" onClick={handleClick} type="button">
          <i class="fa-solid fa-arrow-left"></i>&nbsp; Back
        </span>
        
        {
          countries.map((country, index) => 
          <div class="country-details">
              <img src={`${country.flags.svg}`} alt={`${country.flags.alt}`} />
              <div class="details-text-container">
                <h1>{country.name.common}</h1>
                <div class="details-text">
                  <p><b>Native Name: </b><span class="native-name">{country.name.common}</span></p>
                  <p><b>Population: </b><span class="population">{country.population.toLocaleString('en-IN')}</span></p>
                  <p><b>Region: </b><span class="region">{country.region}</span></p>
                  <p><b>Sub Region: </b><span class="sub-region">{country.subregion}</span></p>
                  <p><b>Capital: </b><span class="capital">{country.capital?.[0]}</span></p>
                  <p>
                    <b>Top Level Domain: </b><span class="top-level-domain">{country.tld.join(', ')}</span>
                  </p>
                  <p><b>Currencies: </b><span class="currencies">{Object.values(country.currencies).map((currency) => currency.name).join(', ')}</span></p>
                  <p><b>Languages: </b><span class="languages">{Object.values(country.languages).join(', ')}</span></p>
                  <p><b>Side: </b><span class="languages">{country.car.side}</span></p>
                </div>
                <div class="border-countries"><b>Translations: </b>&nbsp; 
                {
                  country&&Object.entries(country.translations).map((translation) => 
                    <NavLink to='#'>{translation[1]&&translation[1].common}</NavLink>
                  )
                }
                </div>
                <div class="border-countries"><b>Maps: </b>&nbsp; 
                  <NavLink to='#'>{country.maps&&country.maps.googleMaps}</NavLink>
                  <NavLink to='#'>{country.maps&&country.maps.openStreetMaps}</NavLink>
                </div>
                {/* <div class="border-countries"><b>Border Countries: </b>&nbsp; 
                {
                  
                  country.border&&country.borders.forEach((border) => {
                    fetch(`https://restcountries.com/v3.1/alpha/${border}`)
                      .then((res) => res.json())
                      .then(([borderCountry]) =>{  
                        console.log('borderCountry', borderCountry);
                      }
                    )
                    // return <NavLink to={`/detail/?name=${borderCountry.name&&borderCountry.name.common}`}>{borderCountry.name&&borderCountry.name.common}</NavLink>
                  })
                }
                </div> */}
              
              </div>
            </div>
          )
        }
          
        </div>
    </main>
  </>);
}

export default Detail;
