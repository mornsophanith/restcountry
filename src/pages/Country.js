import React, { useState } from 'react';
import '../App.css';
import { NavLink } from 'react-router-dom';

function Link(props) {
  return <NavLink {...props}>
    {props.children}
  </NavLink>
}

function Country() {
  const [countries, setCountries] = useState([]);
  const recordPerPage = 10;
  const [currentPage, setCurrentPage] = useState(1);
  const lastIndex = currentPage * recordPerPage;
  const firstIndex = lastIndex - recordPerPage;
  const records = countries.slice(firstIndex, lastIndex);
  const npage = Math.ceil(countries.length / recordPerPage);
  const numbers = [...Array(npage + 1).keys()].slice(1);

  let timer = null;

  React.useEffect(() => {
    fetch('https://restcountries.com/v3.1/all')
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          setCountries(data);
        }
    })
    // eslint-disable-next-line
  }, []);

  function dakMode() {
    document.body.classList.toggle('dark')
  }

  const onChangeSearch = (e) => {
    clearTimeout(timer);

    timer = setTimeout(() => {
      const search = e.target.value.toLowerCase();
      if (e.target.value.toLowerCase()) {
        fetch(`https://restcountries.com/v3.1/name/${search}`)
          .then((res) => res.json())
          .then((data) => {
            console.log('data', data);
            if (data) {
              setCountries(data);
            }
        }) 
      } else {
        fetch('https://restcountries.com/v3.1/all')
          .then((res) => res.json())
          .then((data) => {
            if (data) {
              setCountries(data);
            }
        })
      }
    }, 1000);
  }

  const filterRegion = (e) =>   {
    const filterByRegion = e.target.value;
    fetch(`https://restcountries.com/v3.1/region/${filterByRegion}`)
    .then((res) => res.json())
    .then((data) => {
      if (data) {
        setCountries(data);
      }
    })
  }

  function prePage(){
    if(currentPage !== firstIndex) {
      setCurrentPage(currentPage - 1)
    }
  }
  function nextPage(){
    if(currentPage !== lastIndex) {
      setCurrentPage(currentPage + 1)
    }
  }
  function changeCPage(id){
    setCurrentPage(id)
  }

  return (<>
    <header className="header-container">
        <div className="header-content">
          <h2 className="title"><a href="/">Countries in the world!</a></h2>
          <p className="theme-changer" onClick={dakMode}><i className="fa-regular fa-moon"></i>&nbsp;&nbsp;Dark Mode</p>
        </div>
      </header>
      <main>
        <div className="search-filter-container">
          <div className="search-container">
            <i className="fa-solid fa-magnifying-glass"></i>
            <input type="text" placeholder="Search for a country..." onChange={onChangeSearch}/>
          </div>
          <select className="filter-by-region" onChange={filterRegion}>
            <option hidden>Filter by Region</option>
            <option value="Africa">Africa</option>
            <option value="America">America</option>
            <option value="Asia">Asia</option>
            <option value="Europe">Europe</option>
            <option value="Oceania">Oceania</option>
          </select>
        </div>
        <div className="countries-container">
          {
            records.map((country, index) =>
              <Link to={`/detail/?name=${country.name.common}`} className='country-card' key={index}>
                <img src={`${country.flags.svg}`} alt={country.flags} />
                <div className="card-text">
                    <h3 className="card-title">{country.name.common}</h3>
                    <p><b>Population: </b>{country.population.toLocaleString()}</p>
                    <p><b>Region: </b>{country.region}</p>
                    <p><b>Capital: </b>{country.capital?.[0]}</p>
                </div>
              </Link>
            )
          }
        </div>
        {
          records ? (
            <nav>
              <ul className='pagination'>
                <li className='page-item'>
                  <a href='/#' className='page-link' onClick={prePage}>Prev</a>
                </li>
                {
                  numbers.map((n,i) => (
                    <li className={`page-item ${currentPage === n ? 'active' : ''}`} key={i}> 
                      <a href='/#' className='page-link' onClick={() => changeCPage(n)}>{n}</a>
                    </li>
                  ))
                }
                <li className='page-item'>
                  <a href='/#' className='page-link' onClick={nextPage}>Next</a>
                </li>
              </ul>
            </nav>
          )
          : ''
        }
       
      </main>
  </>);
}

export default Country;
