import React from 'react';
import { Route, Routes } from 'react-router-dom';
import Country from './pages/Country';
import Detail from './pages/Detail';
function App() {
  return (
    <Routes>
      <Route exact path="/" element={<Country />}></Route>
      <Route exact path="/detail" element={<Detail />}></Route>
    </Routes>
);
}

export default App;
